'''
imageread usage example script
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''

import matplotlib.pyplot as plt

from myapp import readImage
from myapp import LENA

print(LENA)
data = readImage(LENA)
plt.imshow(data.view(('u1', 3)))
plt.axis('off')
plt.show(block=False)
plt.show()
