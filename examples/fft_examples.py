'''
fft usage example script
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''

import numpy as np
import matplotlib.pyplot as plt

from myapp import readImage, LENA, FFT, grayscaleLuminosityTransform


print(LENA)
data = readImage(LENA)
FData = FFT(data)
f, axarr = plt.subplots(2, 2)
axarr[0, 0].imshow(data.view(('u1', 3)))
axarr[0, 0].set_title("Original")
axarr[0, 0].set_axis_off()

axarr[0, 1].imshow(np.log(np.abs(np.fft.fftshift(FData['r'])**2)), cmap='gray')
axarr[0, 1].set_title("FFT of the red component")
axarr[0, 1].set_axis_off()

axarr[1, 0].imshow(np.log(np.abs(np.fft.fftshift(FData['g'])**2)), cmap='gray')
axarr[1, 0].set_title("FFT of the green component")
axarr[1, 0].set_axis_off()

axarr[1, 1].imshow(np.log(np.abs(np.fft.fftshift(FData['b'])**2)), cmap='gray')
axarr[1, 1].set_title("FFT of the blue component")
axarr[1, 1].set_axis_off()
plt.show(block=False)
plt.show()

G = grayscaleLuminosityTransform(data)
FFTG = FFT(G)
print(FFTG.shape)
print(FFTG.dtype)
print('({0.real:.2f} + {0.imag:.2f}i)'.format(FFTG[0, 0]))
