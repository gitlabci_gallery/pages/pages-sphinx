'''
rgb2grayscale usage example script
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''

import matplotlib.pyplot as plt

from myapp import readImage
from myapp import grayscaleLuminosityTransform
from myapp import grayscaleLightnessTransform
from myapp import grayscaleAverageTransform
from myapp import LENA

print(LENA)
data = readImage(LENA)
G1 = grayscaleLuminosityTransform(data)
G2 = grayscaleLightnessTransform(data)
G3 = grayscaleAverageTransform(data)

f, axarr = plt.subplots(2, 2)
axarr[0, 0].imshow(data.view(('u1', 3)))
axarr[0, 0].set_title("Original")
axarr[0, 0].set_axis_off()
axarr[0, 1].imshow(G1, cmap='gray')
axarr[0, 1].set_title("Luminosity")
axarr[0, 1].set_axis_off()
axarr[1, 0].imshow(G2, cmap='gray')
axarr[1, 0].set_title("Lightness")
axarr[1, 0].set_axis_off()
axarr[1, 1].imshow(G3, cmap='gray')
axarr[1, 1].set_title("Average")
axarr[1, 1].set_axis_off()
plt.show(block=False)
plt.show()
