'''
Unit tests of myapp.transformation.fft

Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''
import unittest
import numpy.testing as testing
from myapp import LENA, readImage, FFT


class TestFft(unittest.TestCase):

    # Called before the tests.
    def setUp(self):
        print('Start TestFft')

    # Called after the tests.
    def tearDown(self):
        print('TestFft done.')

    def testNdArrayFFT(self):
        data = readImage(LENA)
        GF = FFT(data)
        GF_red_first_elem_str = '({0.real:.2f} + {0.imag:.2f}i)'.format(
            GF['r'][0, 0])
        GF_first_elem_str_goldvalue = "(46445707.00 + 0.00i)"
        testing.assert_string_equal(GF_red_first_elem_str,
                                    GF_first_elem_str_goldvalue)

    def testException(self):
        self.assertRaises(AttributeError, FFT, "Bad parameter")


if __name__ == "__main__":
    unittest.main()
