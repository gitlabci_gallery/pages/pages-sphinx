"""**myapp** is a is a simple application example used for training.

This package is Free software, released under the GNU General Public License
(GPLv3).
"""


# As the application contains only a few functions,
# we can reduce the namespaces with the following imports and
#  __all__ definition.
# Note that this is not always a good practice.

from myapp.color.rgb2grayscale import grayscaleAverageTransform
from myapp.color.rgb2grayscale import grayscaleLightnessTransform
from myapp.color.rgb2grayscale import grayscaleLuminosityTransform
from myapp.data import LENA
from myapp.io.imageread import readImage
from myapp.transformations.fft import FFT
from _csv import __version__

__all__ = ["grayscaleAverageTransform", "grayscaleLightnessTransform",
           "grayscaleLuminosityTransform", "LENA", "readImage", "FFT"]

# Be careful that this version number matches the one of the setup.py file.
__version__ = "1.0a1"
