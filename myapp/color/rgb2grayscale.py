'''
Module used to transform color images data to grayscale 2darray
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''

import numpy as np


def grayscaleLuminosityTransform(data):
    """Transform an rgb numpy recarray to grayscale 2darray with the luminosity
    transform

    - Usage:

         | ``G = grayscaleLuminosityTransform(data)``

    - Input parameters:

    :param numpy.record data: rgb image data\
    with three uint8 fields 'r', 'g' and 'b'.

    - Output parameters:

    :return: numpy 2darray of unsigned char.
    :rtype: numpy.ndarray

    - notes:

    The luminosity transform affects each grayscale pixel to 0.21 R + 0.72 G +
    0.07 B

    - Examples:

    >>> from myapp.io import imageread as imr
    >>> from myapp.data import LENA
    >>> data = imr.readImage(LENA)
    >>> G = grayscaleLuminosityTransform(data)
    >>> print(G.shape)
    (512, 512)
    >>> print(G.dtype)
    uint8
    >>> print (G[0:10,0])
    [150 150 150 150 150 150 150 150 144 144]
    """
    return np.asarray((0.21 * (data['r'] / 255) + 0.72 * (data['g'] / 255) +
                       0.07 * (data['b'] / 255)) * 255, dtype='u1')


def grayscaleLightnessTransform(data):
    """Transform an rgb numpy recarray to grayscale 2darray with the lightness
    transform

    - Usage:

         | ``G = grayscaleLightnessTransform(data)``

    - Input parameters:

    :param numpy.record data: rgb image data\
    with three uint8 fields 'r', 'g' and 'b'.

    - Output parameters:

    :return: numpy 2darray of unsigned char.
    :rtype: numpy.ndarray

    - notes:

    The lightness transform affects each grayscale pixel to max(R, G, B)/2

    - Examples:

    >>> from myapp.io import imageread as imr
    >>> from myapp.data import LENA
    >>> data = imr.readImage(LENA)
    >>> G = grayscaleLightnessTransform(data)
    >>> print(G.shape)
    (512, 512)
    >>> print(G.dtype)
    uint8
    >>> print (G[0:10,0])
    [171 171 171 171 171 171 171 171 163 163]
    """
    dataFlat = data.view((data.dtype[0], len(data.dtype.names)))
    return np.asarray((np.max(dataFlat, axis=2).astype('u2') +
                       np.min(dataFlat, axis=2).astype('u2')) // 2, dtype='u1')


def grayscaleAverageTransform(data):
    """Transform an rgb numpy recarray to grayscale 2darray with the average
    transform

    - Usage:

         | ``G = grayscaleAverageTransform(data)``

    - Input parameters:

    :param numpy.record data: rgb image data\
    with three uint8 fields 'r', 'g' and 'b'.

    - Output parameters:

    :return: numpy 2darray of unsigned char.
    :rtype: numpy.ndarray

    - notes:

    The average transform affects each grayscale pixel to (R + G + B)/3

    - Examples:

    >>> from myapp.io import imageread as imr
    >>> from myapp.data import LENA
    >>> data = imr.readImage(LENA)
    >>> G = grayscaleAverageTransform(data)
    >>> print(G.shape)
    (512, 512)
    >>> print(G.dtype)
    uint8
    >>> print (G[0:10,0])
    [158 158 158 158 158 158 158 158 150 150]
    """
    dataFlat = data.view((data.dtype[0], len(data.dtype.names)))
    return np.asarray(np.mean(dataFlat, axis=2, dtype='i'), dtype='u1')


if __name__ == "__main__":  # pragma: no cover
    import doctest
    import myapp.color.rgb2grayscale
    doctest.testmod(myapp.color.rgb2grayscale)
