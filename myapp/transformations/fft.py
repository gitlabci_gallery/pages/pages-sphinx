'''
FFT calculation module
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''

import numpy as np


def FFT(data):
    """Return the FFT of an image data

    - Usage:

         | ``FData = FFT(data)``

    - Input parameters:

    :param data: image data.
    :type data: numpy.recarray or numpy.ndarray 

    - Output parameters:

    :return: numpy.recarray or numpy.ndarray of complex 128.
    :rtype: numpy.recarray or numpy.ndarray

    - Examples:

    >>> from myapp.io import imageread as imr
    >>> from myapp.data import LENA
    >>> data = imr.readImage(LENA)
    >>> from myapp.color import rgb2grayscale as rg
    >>> G = rg.grayscaleLuminosityTransform(data)
    >>> FFTG = FFT(G)
    >>> print(FFTG.shape)
    (512, 512)
    >>> print(FFTG.dtype)
    complex128
    >>> print ('({0.real:.2f} + {0.imag:.2f}i)'.format(FFTG[0,0]))
    (29464029.00 + 0.00i)
    """
    if type(data) is np.recarray:
        FData = np.recarray(data.shape, dtype=[
                            (nm, np.complex128) for nm in data.dtype.names])
        for nm in data.dtype.names:
            FData[nm] = np.fft.fftn(data[nm])
        return FData
    if type(data) is np.ndarray:
        return np.fft.fftn(data)
    raise AttributeError("Unknown input data type")


if __name__ == "__main__":  # pragma: no cover
    import doctest
    import myapp.transformations.fft
    doctest.testmod(myapp.transformations.fft)
