'''
Module used to read images from files
Created on 6 dec. 2022

@author: denis.arrivault@inria.fr
'''
import numpy as np
import skimage.io as io


def readImage(fileName):
    """Read image files

    - Usage:

         | ``I = readImage(fileName)``

    - Input parameters:

    :param String fileName: image file full name.

    - Output parameters:

    :return: record array with three color components 'r', 'g' and 'b'. Each\
    component is an array of unsigned char.
    :rtype: numpy.recarray

    - Examples:

    >>> from myapp.data import LENA
    >>> data = readImage(LENA)
    >>> print(data.shape)
    (512, 512)
    >>> print(data.dtype)
    (numpy.record, [('r', 'u1'), ('g', 'u1'), ('b', 'u1')])
    >>> print(data.view(('u1', 3))[0,0:10,:])
    [[222 133 121]
     [222 133 121]
     [226 137 129]
     [222 133 121]
     [222 133 121]
     [222 125 113]
     [222 133 121]
     [222 133 121]
     [226 137 129]
     [222 133 121]]

    """
    image = io.imread(fileName)
    data = np.recarray((image.shape[0], image.shape[1]), dtype=[
                       ('r', 'u1'), ('g', 'u1'), ('b', 'u1')])
    data['r'] = image[:, :, 0]
    data['g'] = image[:, :, 1]
    data['b'] = image[:, :, 2]
    return data


if __name__ == "__main__":  # pragma: no cover
    import doctest
    import myapp.io.imageread
    doctest.testmod(myapp.io.imageread)
