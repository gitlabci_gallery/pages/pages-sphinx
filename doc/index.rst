.. myapp documentation master file, created by
   sphinx-quickstart on Wed Dec  7 10:37:12 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to myapp's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   myapp

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
