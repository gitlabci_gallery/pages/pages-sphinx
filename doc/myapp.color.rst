myapp.color package
===================

Submodules
----------

myapp.color.rgb2grayscale module
--------------------------------

.. automodule:: myapp.color.rgb2grayscale
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: myapp.color
   :members:
   :undoc-members:
   :show-inheritance:
