myapp package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   myapp.color
   myapp.data
   myapp.io
   myapp.transformations

Module contents
---------------

.. automodule:: myapp
   :members:
   :undoc-members:
   :show-inheritance:
