This gitlab project gives an example of generating a sphinx documentation for a python application.

# The example application

The application **myapp** is a small python application that uses scikit-image to read and write images. 
It also includes a RGB to grayscale transformation, a fft and a color image of Lena in the *data* sub-folder.

At the root of the project one will find a *tests* folders including some unit tests done with the unittest
framework and an *example* folder with three use cases.

The application can be built with [*poetry*](https://python-poetry.org/) by typing:

```bash
poetry install # to build the environment
poetry build # to build the dist folder if one wants to distribute the application
poetry shell # to start the dedicated python virtual environment
```

When the virtual environment is launched, the unit tests and the coverage can be obtained with:

```bash
python -m coverage run -m unittest
python -m coverage report
```

If the `poetry shell` is not activated do prefix the previous command with `poetry run [...]`.

# The Sphinx documentation generation

The documentation has been generated with sphinx-apidoc in the *doc* folder. To update it, cd to *doc* and type:

```bash
sphinx-apidoc -o . ../myapp
make html
```

The documentation is generated in the *_build/html* sub-folder.

For more documentation please visit the Sphinx documentation website [here](https://www.sphinx-doc.org/en/master/index.html).

# Continuous integration

In the CI process we put two stages: build and deploy. At the first stage the application is built with poetry
and the documentation is generated. at the second stage the html documentation is copied in the page `public`
folder.

Note that we use the last ubuntu image in which we add python3, curl and make. Curl is used for installing the
last version of poetry. 

Before launching the generation be sure you activated the CI/CD option in the `Settings/General/Visibility, project features, permissions` section of your
gitlab project. You have also to enable shared runners for the project in the `Settings/CI/CD/Runners` section.

To see the sphinx documentation open the `Settings/Pages` section, you will find the url.

That's it!